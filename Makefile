TEST_IMAGE?=vault-helm-test
TMPDIR?=./tmpdir
SERVICE?=vault-server-tls
NAMESPACE?=vault
SECRET_NAME?=vault-server-tls
CSR_NAME?=vault-server-csr

test-image:
	@docker build --rm -t '$(TEST_IMAGE)' -f $(CURDIR)/test/docker/Test.dockerfile $(CURDIR)

test-unit:
	@docker run -it -v ${PWD}:/helm-test vault-helm-test bats /helm-test/test/unit

test-acceptance:
	@docker run -it -v ${PWD}:/helm-test vault-helm-test bats /helm-test/test/acceptance

test-bats: test-unit test-acceptance

test: test-image test-bats

replace_csr:
	@sed -ie "s|REPLACEME|`cat ${TMPDIR}/server.csr | base64 | tr -d '\n'`|g" ${TMPDIR}/csr.yaml

create_csr:
	@kubectl create -f ${TMPDIR}/csr.yaml

get_cert:
	@if (test -f "${TMPDIR}/vault.crt") ; \
		then echo "vault.crt already exists, skipping"; \
	else \
		kubectl get csr ${CSR_NAME} -o jsonpath='{.status.certificate}' | openssl base64 -d -A -out ${TMPDIR}/vault.crt ; \
	fi && \
	if (test -f "${TMPDIR}/vault.ca") ; \
		then echo "CA already exported"; \
	else \
		kubectl config view --raw --minify --flatten -o jsonpath='{.clusters[].cluster.certificate-authority-data}' | base64 -D > ${TMPDIR}/vault.ca ; \
	fi

create_secret:
	@if (kubectl get secrets -n vault | grep ${SECRET_NAME}); \
		then echo "secret already created"; \
	else \
		kubectl create secret generic ${SECRET_NAME} --namespace ${NAMESPACE} \
			--from-file=vault.key=${TMPDIR}/vault.key \
			--from-file=vault.crt=${TMPDIR}/vault.crt \
			--from-file=vault.ca=${TMPDIR}/vault.ca ; \
	fi

kube:
	@mkdir -p ${TMPDIR} && \
	if test -f "${TMPDIR}/vault.key" ; \
		then echo "key already exists"; \
	else \
		openssl genrsa -out ${TMPDIR}/vault.key 2048; \
	fi && \
	if test -f "${TMPDIR}/csr.conf"; \
		then echo "csr.conf Already exists"; \
	else \
		printf \
	'[req]'\\n\
	'req_extensions = v3_req'\\n\
	'distinguished_name = req_distinguished_name'\\n\
	[req_distinguished_name]\\n\
	'[ v3_req ]'\\n\
	'basicConstraints = CA:FALSE'\\n\
	'keyUsage = nonRepudiation, digitalSignature, keyEncipherment'\\n\
	'extendedKeyUsage = serverAuth'\\n\
	'subjectAltName = @alt_names'\\n\
	[alt_names]\\n\
	'DNS.1 = ${SERVICE}'\\n\
	'DNS.2 = ${SERVICE}.${NAMESPACE}'\\n\
	'DNS.3 = ${SERVICE}.${NAMESPACE}.svc'\\n\
	'DNS.4 = ${SERVICE}.${NAMESPACE}.svc.cluster.local'\\n\
	'IP.1 = 127.0.0.1' >> ${TMPDIR}/csr.conf ; \
	fi &&  \
	if (kubectl get namespaces | grep ${NAMESPACE} ); \
		then echo "namespace already exists"; \
	else \
		kubectl create namespace ${NAMESPACE}; \
	fi && \
	if test -f "${TMPDIR}/server.csr" ; \
		then echo "csr already generated" ; \
	else \
		openssl req -new -key ${TMPDIR}/vault.key -subj "/CN=${SERVICE}.${NAMESPACE}.svc" -out ${TMPDIR}/server.csr -config ${TMPDIR}/csr.conf ; \
	fi && \
	if test -f "${TMPDIR}/csr.yaml" ; \
		then echo "csr YAML exists"; \
	else \
		printf \
	'apiVersion: certificates.k8s.io/v1beta1'\\n\
	'kind: CertificateSigningRequest'\\n\
	'metadata:'\\n\
	'  name: ${CSR_NAME}'\\n\
	'spec:'\\n\
	'  groups:'\\n\
	'  - system:authenticated'\\n\
	'  request: REPLACEME'\\n\
	'  usages:'\\n\
	'  - digital signature'\\n\
	'  - key encipherment'\\n\
	'  - server auth' >> ${TMPDIR}/csr.yaml ; \
	fi && \
	make replace_csr ; \
	make create_csr ; \
	echo "sleeping to wait for creation"; \
	sleep 20; \
	kubectl certificate approve ${CSR_NAME}; \
	make get_cert ; \
	make create_secret; \
	make install;

clean: uninstall
	@if (test -f ${TMPDIR}/csr.yaml) ; \
		then 	kubectl delete -f ${TMPDIR}/csr.yaml ; \
	fi && \
	rm -rf ${TMPDIR}/ ; \
	if (kubectl get secrets -n vault | grep ${SECRET_NAME}); \
		then kubectl delete secret -n vault ${SECRET_NAME}; \
	else \
		echo "Secret has already been deleted" ; \
	fi && \
	if (kubectl get namespaces | grep ${NAMESPACE} ); \
		then kubectl delete namespace ${NAMESPACE}; \
	else \
		 echo "namespace already cleaned"; \
	fi


install:
	helm install vault ./ -n vault

uninstall:
	helm uninstall vault -n vault

reinstall: uninstall install

exec:
	kubectl exec -it -n vault  vault-0  -- /bin/sh

.PHONY: test-docker
