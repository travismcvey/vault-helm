# Vault Helm Chart

This repository contains the official HashiCorp Helm chart for installing
and configuring Vault on Kubernetes. This chart supports multiple use
cases of Vault on Kubernetes depending on the values provided.

For full documentation on this Helm chart along with all the ways you can
use Vault with Kubernetes, please see the
[Vault and Kubernetes documentation](https://www.vaultproject.io/docs/platform/k8s/index.html).

## Prerequisites

To use the charts here, [Helm](https://helm.sh/) must be installed in your
Kubernetes cluster. Setting up Kubernetes and Helm and is outside the scope
of this README. Please refer to the Kubernetes and Helm documentation.

The versions required are:

  * **Helm 3.0+** - This is the earliest version of Helm tested. It is possible
    it works with earlier versions but this chart is untested for those versions.
  * **Kubernetes 1.9+** - This is the earliest version of Kubernetes tested.
    It is possible that this chart works with earlier versions but it is
    untested. Other versions verified are Kubernetes 1.10, 1.11.

## Usage

For now, we do not host a chart repository. To use the charts, you must
download this repository and unpack it into a directory. Either
[download a tagged release](https://github.com/hashicorp/vault-helm/releases) or
use `git checkout` to a tagged release.
Assuming this repository was unpacked into the directory `vault-helm`, the chart can
then be installed directly:

    helm install ./vault-helm

Please see the many options supported in the `values.yaml`
file. These are also fully documented directly on the
[Vault website](https://www.vaultproject.io/docs/platform/k8s/helm.html).

## Internal Notes

Make has been extended to create a simple development environment for the helm chart. You will need a kubernetes cluster and helm.  For local [minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/).

Once complete, Make will create the certificates and required namespace and secrets. While these can be changed, it is highly suggested to keep defaults, otherwise vaules.yaml will need to be modified.

  bash
  make kube
  make install

### Executing into the container

  bash
  make exec

This will connect you into the container.

  bash
  vault status
  vault operator init

### Changes

To re-install the helm chart run:

  bash
  make reinstall

### Clean up

To remove all items created by make in your filesystem and the kubernetes cluster please run:

  bash
  make clean